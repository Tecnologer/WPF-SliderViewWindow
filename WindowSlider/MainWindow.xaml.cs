﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WindowSlider
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point initialPoint;
        private double pointInsideX;
        private double pointInsideY;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            initialPoint = e.GetPosition(this);
            pointInsideX = Math.Abs(ColLeft.ActualWidth - initialPoint.X);
            pointInsideY = Math.Abs(RowTop.ActualHeight - initialPoint.Y);
            System.Diagnostics.Debug.WriteLine("Border center");
            e.Handled = true;
        }

        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
            {
                Mouse.OverrideCursor = Cursors.SizeAll;
                var currentPoint = e.GetPosition(this);
                SliderLeft.Value = Math.Abs(currentPoint.X - pointInsideX);
                SliderRight.Value = Math.Abs(SliderLeft.Value + ColCenter.ActualWidth);
                ColLeft.Width = new GridLength(SliderLeft.Value);
                RowTop.Height = new GridLength(Math.Abs(currentPoint.Y - pointInsideY));
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (SliderRight.Value <= SliderLeft.Value)
                SliderRight.Value = SliderLeft.Value+10;

            ColCenter.Width = new GridLength(Math.Abs(SliderLeft.Value - SliderRight.Value));
        }

        private void root_ContentRendered(object sender, EventArgs e)
        {
            SliderLeft.Value = ColLeft.ActualWidth;
            SliderRight.Value = SliderLeft.Value + ColCenter.ActualWidth;
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        private void root_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SliderLeft.Value = ColLeft.ActualWidth;
            SliderRight.Value = SliderLeft.Value + ColCenter.ActualWidth;
        }
    }
}
